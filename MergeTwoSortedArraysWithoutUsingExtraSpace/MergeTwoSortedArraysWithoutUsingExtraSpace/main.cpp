#include <iostream>
#include <vector>

using namespace std;

class Engine
{
    private:
        vector<int> arr1;
        vector<int> arr2;
    
        void updateArr2(int num)
        {
            int len = (int)arr2.size();
            int i   = 0;
            while(i < (len - 1) && arr2[i] < num)
            {
                arr2[i] = arr2[i+1];
                i++;
            }
            i = !i ? i : --i;
            arr2[i] = num;
        }
    
        void mergeArrays()
        {
            int len = (int)arr2.size();
            for(int i = 0 ; i < len ; i++)
            {
                arr1.push_back(arr2[i]);
            }
        }
    
        void displayMergedArray()
        {
            int len  = (int)arr1.size();
            for(int i = 0 ; i < len ; i++)
            {
                cout<<arr1[i]<<" ";
            }
            cout<<endl;
        }
    
    public:
        Engine(vector<int> a1 , vector<int> a2)
        {
            arr1 = a1;
            arr2 = a2;
        }
    
        void mergeSortedArrays()
        {
            int len = (int)arr1.size();
            for(int i = 0 ; i < len ; i++)
            {
                if(arr1[i] > arr2[0])
                {
                    int temp = arr1[i];
                    arr1[i]  = arr2[0];
                    updateArr2(temp);
                }
            }
            mergeArrays();
            displayMergedArray();
        }
};

int main(int argc, const char * argv[])
{
    vector<int> arr1 = {-3 , 5 , 7 , 10 , 11};
    vector<int> arr2 = {-1 , 2 , 6 , 12};
    Engine      e    = Engine(arr1 , arr2);
    e.mergeSortedArrays();
    return 0;
}
